<?php

require "vendor/autoload.php";

// Routing

$page = "home";

if (isset($_GET["p"])) {
    $page = $_GET["p"];
}

// Fake

$faker = Faker\Factory::create();



// Rendu du template

$loader = new Twig_Loader_Filesystem(__DIR__ . "/templates");

$twig = new Twig_Environment($loader, [
    "cache" => false, // __DIR__ . "/tmp"
]);

if ($page === "home") {
    echo $twig->render("home.twig", [
        "companyName" => $faker->company, 
        "catchPhrase" => $faker->realText($maxNbChars = 50, $indexSize = 2),
        "productAdjective" => $faker->randomElement(["good", "bad", "super", "cool", "fast"]),
        "productName" => $faker->randomElement(["chairs", "computer", "banana"]),
        "productMaterial" => $faker->randomElement(["iron", "gold", "wood"]),
        "url" => $faker->url,
        "img" => $faker->imageUrl($width = 400, $height = 260)
        ]);
}

?>